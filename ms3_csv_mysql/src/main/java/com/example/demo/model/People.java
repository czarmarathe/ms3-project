package com.example.demo.model;

import java.util.Arrays;

public class People {
	
	public String first_name;
	public String last_name;
	public String email;
	public String gender;
	public String image;
	public byte[] blob_image;
	public String payment;
	public String cost;
	public String b1;
	public boolean b1_out;
	public String b2;
	public boolean b2_out;
	public String location;
	
	static public int data_received = 0;
	static public int data_successful = 0;
	static public int data_failed = 0;
	
	
	public People() {
		
	}
	
	public People(People s) {
		
		first_name 	= s.first_name;
		last_name 	= s.last_name;
		email 		= s.email;
		gender 		= s.gender;
		image 		= s.image;
		blob_image	= s.blob_image;
		payment 	= s.payment;
		cost 		= s.cost;
		b1 			= s.b1;
		b2 			= s.b2;
		location 	= s.location;
	}
	


	public People(String first_name, String last_name, String email, String gender, String image, byte[] blob_image,
			String payment, String cost, String b1, boolean b1_out, String b2, boolean b2_out, String location) {
		super();
		this.first_name = first_name;
		this.last_name = last_name;
		this.email = email;
		this.gender = gender;
		this.image = image;
		this.blob_image = blob_image;
		this.payment = payment;
		this.cost = cost;
		this.b1 = b1;
		this.b1_out = b1_out;
		this.b2 = b2;
		this.b2_out = b2_out;
		this.location = location;
	}

	public String getFirst_name() {
		return first_name;
	}


	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}


	public String getLast_name() {
		return last_name;
	}


	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getGender() {
		return gender;
	}


	public void setGender(String gender) {
		this.gender = gender;
	}


	public String getImage() {
		return image;
	}


	public void setImage(String image) {
		this.image = image;
	}


	public byte[] getBlob_image() {
		return blob_image;
	}

	
	public void setBlob_image(byte[] blob_image) {
		this.blob_image = blob_image;
	}

	public String getPayment() {
		return payment;
	}


	public void setPayment(String payment) {
		this.payment = payment;
	}


	public String getCost() {
		return cost;
	}


	public void setCost(String cost) {
		this.cost = cost;
	}


	public String getB1() {
		return b1;
	}

	public void setB1(String b1) {
		this.b1 = b1;
	}

	public boolean isB1_out() {
		return b1_out;
	}

	public void setB1_out(boolean b1_out) {
		this.b1_out = b1_out;
	}

	public String getB2() {
		return b2;
	}

	public void setB2(String b2) {
		this.b2 = b2;
	}

	public boolean isB2_out() {
		return b2_out;
	}

	public void setB2_out(boolean b2_out) {
		this.b2_out = b2_out;
	}

	public String getLocation() {
		return location;
	}


	public void setLocation(String location) {
		this.location = location;
	}


	@Override
	public String toString() {
		return "People [first_name=" + first_name + ", last_name=" + last_name + ", email=" + email + ", gender="
				+ gender + ", image=" + image + ", blob_image=" + Arrays.toString(blob_image) + ", payment=" + payment
				+ ", cost=" + cost + ", b1=" + b1 + ", b1_out=" + b1_out + ", b2=" + b2 + ", b2_out=" + b2_out
				+ ", location=" + location + "]";
	}

	public boolean recordIncomplete() {
		if (first_name.equals("") || last_name.equals("") || email.equals("") || gender.equals("") || image.equals("") || 
				b1.equals("")	||	b2.equals("")	||	payment.equals("") ||	cost.equals("") || location.equals("") ) {
			return true;
		}
		else {
			return false;
		}
	}


	
	
}
