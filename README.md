
**This document summarizes details and contents of the project**

This folder contains 2 directories :

1. **MySQL** - Contains files related to database and instructions on how to use the db with project
2. **ms3_csv_mysql** - Project directory containing Spring Boot Batch application which parses CSV data and write relevant info to MySQL Db and other CSV file

Run the project by executing : Ms3CsvMysqlApplication.java
---

## Note :

1. In order to ensure better readability, the columns A-J based on their values are mapped and used as follows :
	A -> first_name	,		B -> last_name	,		C -> email	,		D -> gender ,
	E -> image	,			F -> payment	,		G -> cost	,		H -> b1 ,
	I -> b2	,				J -> location
	
2. Column **'image'** in CSV stores base64 encoded representation of image.
   After reading the desired **Base64** input, program logic transforms the image to an array of bytes for efficient storage in MySQL database.
   Storing this data in BLOB format as **byte array** reduces amount of space consumed and efficient retrieval for server side components.
   
3. The base class is **'People'** which stores all the relevant information read from the reader.

4. The project resource contains **'schema-all.sql'** which is run automatically everytime program is run. It deletes any existing table 'People' from 'ms3' database and creates a new one.

5. The logs are stored under : src/main/resources/logs/

6. Records that don't match are kept in : src/main/resources/scv/bad-data-{timestamp}.csv

7. Fields with comma(,) are writen back to Flat files after parssing them through escape function which adds double quotes (" ") to the element

8. Project contains SOP statements used for manual testing and debugging

9. Boolean values are read as string but stored in database as boolean values

---

## Remaining :

The program effectively parses the first 10 fields and makes a decision to mark it complete or incomplete record.
However, there are certain rows that extend the field count (above 10).
Any attempt by the reader to read additional values in a row(ie. more than 10) generates a parsing error due to lack of class objects in People class.
If we add bogus class memebers to map to these extra rows, they acheive the desired result for that record. However, this results in another parsing error for records that exhaust themselves at count 10.
   
The project fulfils all the requirements from the project except the scenario where item count for a row exceeds 10. 
Anytime attempt to limit the record input in FlatFileItemReader using functions like 'setmaxItemCount(10)' safely exits the reader execution.
   
Possible Solution :
Based on the short reserach, FlatFileItemReader diretly doesnt have any function to check item count in a record.
Using setmaxItemCount(10) to safely exit by skipping the record and restarting the job
   
Due to this under resources, there are 2 source files :
1. ms3Interview.csv - When the program reads this input file, it only reads the first 10 column values and check the validity of records based on the first 10 values
2. ms3Interview_Ext.csv - Program attempts to read all the columns avaiable, thus generates parsing error for rows with more than 10 values
   
---
   
## Log Results :

1. **Reading entries** :
   For reading entries from source file, all the records including the headers (A,B,B...J) considered and passed as a row to the reader.
   
   Headers are present in two different rows in the source files. In our program, they therefore are included as normal data without skipping.
   The result reflects the validation of first 10 fields of every record
   
   Total results includes : All rows + Headers
   Succesful Result included : Rows with proper data
   Failed Result includes : All failed rows + Header row (Validation check)
   
2. **Writing entrier to bad-data.csv**
   While writing the incomplete records to bad-data-{ts}.scv, the header rows are not considered as incorret data.
   Instead they are appended to the header using suitable setheader functions.
   
   
