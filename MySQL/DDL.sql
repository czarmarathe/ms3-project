Create Database ms3;

Use ms3;

Create Table People (
	id int primary key auto_increment,
	first_name	varchar(20),
    last_name	varchar(20),
    email		varchar(40),
    gender		varchar(7),
    image		BLOB,
    payment		varchar(25),
    cost		varchar(7),
    b1			boolean,
    b2			boolean,
    location	varchar(40)
);