There are 2 files present in this folder :
1. DDL - Contains the queries used to create and use Database ms3.
		 Create Table query for 'People'
		 
2. ms3-tabledump - Contains entire db and table strucutre dump


Note : The project resource contains 'schema-all.sql' (src/main/resources/) which creates the desired table even if one already exists
MySQL Engine : InnoDB

Steps to be performed in order to use database with project
1. Create the database 'ms3' or import the 'ms3-tabledump' file
2. Run the project : During startup the table creation query (in schema-all.sql) will run automatically. It deletes any existing table and creates new one